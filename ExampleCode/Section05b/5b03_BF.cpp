/**********************************************
* File: 5b03_BF.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* File for the Brute Force solution to the
* Triple Step problem 
**********************************************/

#include<iostream>
#include<stdlib.h>
#include<ctime>
#include "Array.h"

struct Point{
	int row;
	int col;
	
	Point(): row(0), col(0){}
	Point(int row, int col) : row(row), col(col){}
	
};

/********************************************
* Function Name  : getPath
* Pre-conditions : bool** maze, size_t rows, size_t cols
* Post-conditions: Array<Point>*
*  
********************************************/
Array<Point>* getPath(bool** maze, size_t rows, size_t cols);

/********************************************
* Function Name  : getPath
* Pre-conditions : bool** maze, int row, int col, Array<Point>* path
* Post-conditions: bool
*  
********************************************/
bool getPath(bool** maze, int row, int col, Array<Point>* path);

/********************************************
* Function Name  : printInitPath
* Pre-conditions : bool** maze, size_t rows, size_t cols
* Post-conditions: none
*  
********************************************/
void printInitPath(bool** maze, size_t rows, size_t cols);

/********************************************
* Function Name  : printFinalPath
* Pre-conditions : Array<Point>* thepath
* Post-conditions: none
*  
********************************************/
void printFinalPath(Array<Point>* thepath);

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char**argv
* Post-conditions: int
* 
* This is the main driver function
* Assumes argv[1] is a valid integer 
********************************************/
int main(int argc, char**argv){

	const size_t rows = 5; 
	const size_t cols = 6;
	
	bool **maze;
	maze = new bool*[rows]; // Dynamic array (size cols)
	
	for (size_t i = 0; i < rows; ++i) {
		maze[i] = new bool[cols];
	}
	
    for(size_t i = 0; i < rows; i++){
		for(size_t j = 0; j < cols; j++){
			maze[i][j]=true;
		}
    }
	
	srand(time(NULL));
	size_t randomNum = rand()%(rows*cols);
	
	if(randomNum != 0 && randomNum != (rows*cols-1)){
		maze[randomNum%rows][randomNum%cols] = false;
	}
	else{
		maze[rows%2][cols%2] = false;
	}
	
	// Print the path
	std::cout << "Initial Maze" << std::endl;
	printInitPath(maze, rows, cols);
	
	// Call the function
	//Array<Point>* thePath = getPath(maze, rows, cols);
	
	printFinalPath(getPath(maze, rows, cols));

	return 0;
}

void printInitPath(bool** maze, size_t rows, size_t cols){
	for(size_t i = rows; i > 0; i--){
		for(size_t j = 0; j < cols; j++){
			std::cout << maze[i-1][j] << " ";
		}
		std::cout << std::endl;
	}
}

Array<Point>* getPath(bool** maze, size_t rows, size_t cols){
	Array<Point>* path = new Array<Point>();
	
	if(getPath(maze, rows - 1, cols - 1, path)){
		return path;
	}
	
	return nullptr;
}

bool getPath(bool** maze, int row, int col, Array<Point>* path){
	
	if(col < 0 || row < 0 || !maze[row][col]){
		return false;
	}
	
	bool isAtOrigin = (row == 0) && (col == 0);
	
	if(isAtOrigin || getPath(maze, row, col-1, path) || getPath(maze, row-1, col, path)){
		Point p(row, col);
		path->push_back(p);
		return true;
	}

	return false;
}

void printFinalPath(Array<Point>* thePath){
	
	if(thePath != nullptr){
		std::cout << "Path Found! " << thePath->size() << std::endl;
		size_t pathSize = thePath->size();
		for(size_t i = pathSize; i > 0; i--){
			if(!((*thePath)[i-1].row == 0 && (*thePath)[i-1].col == 0)){
				std::cout << "(" << (*thePath)[i-1].row ;
				std::cout << "," << (*thePath)[i-1].col << ")->"; 
			}
			else{
				std::cout << "(" << (*thePath)[i-1].row ;
				std::cout << "," << (*thePath)[i-1].col << ")"; 
			}
		}
		std::cout << std::endl;
	}
}

