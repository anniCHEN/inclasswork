#ifndef __WORD_OP_H__
#define __WORD_OP_H__
#include <string>
#include <iostream>

using namespace std;

bool contain(string word, char x) {
	string temp = word;
	for (int i = 0; i < temp.size(); i++) {
		if (temp[i] == x) {
			return true;
		}
	}
	return false;
}


string remove(string word, char x) {
	string temp = word;
	string::iterator it;
	it = temp.begin();
	for (int i = 0; i < temp.size(); i++) {
		if (temp[i] == x) {
			temp.erase(it);
			break;
		}
		it++;
	}
	return temp;
}


#endif