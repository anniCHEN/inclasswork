#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>
#include <fstream>

#include "word_op.h"

using namespace std;

int main() {
	char first_letter, second_letter, third_letter, first_C, second_C, third_C, fourth_C, fifth_C;
	char first_extra = ' ';
	char second_extra = ' ';
	vector<string> current_words;
	string license_plate;

	//Input the license plate
	std::cout << "Input a license plate in the form [AABCCCCC] (A is a letter, B can be a letter or nothing C must be either a letter or a digit but no more than 2 letters)" << endl;
	////cin >> license_plate;
	getline(std::cin, license_plate);
	int extra_letters = 0;
	if (license_plate.size() == 9) {
		first_letter = license_plate[0];
		second_letter = license_plate[1];
		third_letter = license_plate[2];
		first_C = license_plate[4];
		second_C = license_plate[5];
		third_C = license_plate[6];
		fourth_C = license_plate[7];
		fifth_C = license_plate[8];
		if (!isupper(first_letter) || !isupper(second_letter) || !isupper(third_letter)) {
			std::cout << "Wrong license plate!" << endl;
			//std::cout << "Not Upper letters in the first part!" << endl;
			return 0;
		}
	}
	else if (license_plate.size() == 8) {
		first_letter = license_plate[0];
		second_letter = license_plate[1];
		third_letter = license_plate[2];
		if (third_letter != ' ') {
			std::cout << "Wrong license plate!" << endl;
			//std::cout << "No space!" << endl;
			return 0;
		}
		first_C = license_plate[3];
		second_C = license_plate[4];
		third_C = license_plate[5];
		fourth_C = license_plate[6];
		fifth_C = license_plate[7];
		if (!isupper(first_letter) || !isupper(second_letter)) {
			std::cout << "Wrong license plate!" << endl;
			//std::cout << "Not Upper letters in the first part!" << endl;
			return 0;
		}
	}
	else {
		std::cout << "Wrong license plate!" << endl;
		//std::cout << "Invalid Length!" << endl;
		return 0;
	}

	for (int i = license_plate.size() - 5; i < license_plate.size(); i++) {
		if (!isalnum(license_plate[i])) {
			std::cout << "Wrong license plate!" << endl;
			//std::cout << "Not Digit or Letter in the second part!" << endl;
			return 0;
		}
		if (isupper(license_plate[i]) && extra_letters == 0) {
			first_extra = license_plate[i];
			extra_letters++;
		}
		else if (isupper(license_plate[i]) && extra_letters == 1) {
			second_extra = license_plate[i];
			extra_letters++;
		}
		else if (isupper(license_plate[i]) && extra_letters == 2) {
			std::cout << "Wrong license plate!" << endl;
			//std::cout << "No more than 2 letters in the second part!" << endl;
			return 0;
		}
	}

	//Show the license plate
	std::cout << "The license plate is: " << license_plate << endl;

	ifstream iFile;
	iFile.open("words.txt");
	vector<string> words;
	while (iFile) {
		string word;
		iFile >> word;
		words.push_back(word);
	}


	for (int i = 0; i < words.size(); i++) {
		bool flag1 = false;
		bool flag2 = false;
		bool flag3 = true;
		bool flag4 = true;
		bool flag5 = true;
		string temp = words[i];

		flag1 = contain(temp, first_letter);
		temp = remove(temp, first_letter);
		flag2 = contain(temp, second_letter);
		temp = remove(temp, second_letter);

		if (isupper(first_extra)) {
			flag3 = contain(temp, first_extra);
			temp = remove(temp, first_extra);
		}

		if (isupper(second_extra)) {
			flag4 = contain(temp, second_extra);
			temp = remove(temp, second_extra);
		}

		if (isupper(third_letter)) {
			flag5 = contain(temp, third_letter);
			temp = remove(temp, third_letter);
		}

		if (flag1 && flag2 && flag3 && flag4 && flag5) {
			if (current_words.empty()) {
				current_words.push_back(words[i]);
			}
			else {
				if (current_words[0].size() == words[i].size()) {
					current_words.push_back(words[i]);
				}
				else if (current_words[0].size() > words[i].size()) {
					current_words.clear();
					current_words.push_back(words[i]);
				}
			}
		}
	}

	std::cout << "The shortest word(s) containing the letters in the license plate is(are): " << endl;
	for (int i = 0; i < current_words.size(); i++) {
		std::cout << current_words[i] << endl;
	}

	return 0;
}