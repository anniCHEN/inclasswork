#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <climits>
#include <ctime>
#include <cassert>
#include "merge.h"
#include "QuadraticProbing.h"

using namespace std;

int main() {
	// Define the array and the target
	int size;
	cout << "Input the size of the array:" << endl;
	cin >> size;
	int *A = new int[size];
	cout << "Input the elements:" << endl;
	for (int i = 0; i < size; i++) {
		cin >> A[i];
	}
	int target;
	cout << "Input the target number:" << endl;
	cin >> target;
	//brute force
	cout << "By brute force: ============" << endl;
	for (int i = 0; i < size; i++) {
		for (int j = i + 1; j < size; j++) {
			if (A[i] + A[j] == target) {
				cout << A[i] << " with index " << i << endl;
				cout << A[j] << " with index " << j << endl;
				cout << "============================" << endl;
			}
		}
	}
	//hash table

	cout << "By hash table: ==============" << endl;
	HashTable<int> h1(target);
	for (int i = 0; i < size; i++) {
		if (A[i] > target) {
			continue;
		}
		else h1.insert(A[i]);
	}
	for (int i = 0; i < target / 2; i++) {
		if (h1.exist(i) && h1.exist(target - i)) {
			h1.display(i);
			h1.display(target - i);
			cout << "============================" << endl;
		}
	}

	//
	//after sort 
	mergesort(A, 0, size - 1);
	int a = 0;
	int b = size - 1;
	cout << "By after sort: =============" << endl;
	while (a < b) {
		if (A[a] + A[b] > target) {
			b--;
		}
		else if (A[a] + A[b] < target) {
			a++;
		}
		else {
			cout << A[a] << " with index " << a << endl;
			cout << A[b] << " with index " << b << endl;
			cout << "============================" << endl;
			a++;
			b--;
		}
	}

	delete[] A;
	return 0;
}