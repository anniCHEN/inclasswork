#ifndef MERGE_H
#define MERGE_H

#include <iostream>
using namespace std;

void merge(int A[], int left, int mid, int right) {
	int temp[right - left + 1];
	int i, j, k;
	i = left;
	j = mid + 1;
	k = 0;
	while (i < mid + 1 && j < right + 1) {
		if (A[i] <= A[j]) temp[k++] = A[i++];
		else temp[k++] = A[j++];
	}
	while (i < mid + 1) {
		temp[k++] = A[i++];
	}
	while (j < right + 1) {
		temp[k++] = A[j++];
	}
	for (int i = left; i < right + 1; i++) {
		A[i] = temp[i - left];
	}
}

void mergesort(int A[], int left, int right) {
	if (left >= right) return;
	int mid = (left + right) / 2;
	mergesort(A, left, mid);
	mergesort(A, mid + 1, right);
	merge(A, left, mid, right);
}

#endif