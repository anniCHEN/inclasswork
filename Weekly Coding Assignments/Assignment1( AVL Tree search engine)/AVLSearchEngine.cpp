/**********************************************
* File: AVLSearchEngine.cpp
* Author: Anni Chen
* Email:achen4@nd.edu
*
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

struct urlSearch {
	std::string url;
	std::string webName;
	urlSearch(std::string url, std::string webName) :url(url), webName(webName) {}

	bool operator< (const urlSearch& rhs) const {
		if (url < rhs.url) return true;
		return false;
	}

	bool operator== (const urlSearch& rhs) const {
		if (url == rhs.url) return true;
		return false;
	}

	bool operator> (const urlSearch& rhs) const {
		if (url > rhs.url) return true;
		return false;
	}
	friend std::ostream& operator<<(std::ostream& outStream, const urlSearch& printSearch);
};

std::ostream& operator<<(std::ostream& outStream, const urlSearch& printSearch) {
	outStream << printSearch.webName << ", " << std::endl;
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*
* This is the main driver program for the
* AVLTree function with strings
********************************************/
int main(int argc, char **argv)
{
	//Create initial AVLTree
	AVLTree<urlSearch> searchEngineAVL;

	urlSearch url1("http://cse.nd.edu", "Notre Dame CSE Homepage");
	urlSearch url2("https://www.google.com/", "Google Homepage");
	urlSearch url3("https://www.google.com/gmail/","Gmail page under Goole");
	urlSearch url4("https://www.google.com/search?safe=strict&ei=JX-VXP6vF8XHjgST2qXACw&q=gmail+sign+up&oq=gmail&gs_l=psy-ab.1.3.0i71l8.0.0..58723...0.0..0.0.0.......0......gws-wiz.I1MOYj0exQg", "Search gmail-sign-up in Google");
	urlSearch url5("https://about.gitlab.com/", "Gitlab introduction page");
	urlSearch url6("https://www.nd.edu/", "Notre Dame Homepage");
	urlSearch url7("https://sakai.nd.edu/", "Sakai Homepage");
	urlSearch url8("https://sakailogin.nd.edu/portal/site/SP19-CSE-20312-SS-02", "Sakai CSE-20312 Homepage");
	urlSearch url9("https://www.facebook.com/", "Facebook Homepage");
	urlSearch url10("https://cse.nd.edu/undergraduates", "Notre Dame CSE undergraduates page");

	searchEngineAVL.insert(url1);
	searchEngineAVL.insert(url2);
	searchEngineAVL.insert(url3);
	searchEngineAVL.insert(url4);
	searchEngineAVL.insert(url5);
	searchEngineAVL.insert(url6);
	searchEngineAVL.insert(url7);
	searchEngineAVL.insert(url8);
	searchEngineAVL.insert(url9);
	searchEngineAVL.insert(url10);

	searchEngineAVL.printTree();

	searchEngineAVL.removeRoot();

	searchEngineAVL.printTree();


	return 0;
}
