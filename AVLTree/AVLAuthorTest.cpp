/**********************************************
* File: AVLAuthorTest.cpp
* Author: Anni Chen
* Email:achen4@nd.edu
*
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

// Struct goes here
struct Author {
	std::string FirstName;
	std::string LastName;

	Author(std::string FirstName, std::string LastName) :FirstName(FirstName),LastName(LastName){}
	bool operator< (const Author& rhs) const {

		if (LastName < rhs.LastName)
			return true;
		else if (LastName == rhs.LastName) {
			if (FirstName < rhs.FirstName)
				return true;
		}
		return false;
	}
	bool operator== (const Author& rhs) const {

		if (LastName != rhs.LastName)
			return false;
		else {
			if (FirstName != rhs.FirstName)
				return false;
		}
	}
	friend std::ostream& operator<<(std::ostream& outStream, const Author& printAuthor);
};

std::ostream& operator<<(std::ostream& outStream, const Author& printAuth) {
	outStream << printAuth.LastName << ", " << printAuth.FirstName << std::endl;
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*
* This is the main driver program for the
* AVLTree function with strings
********************************************/
int main(int argc, char **argv)
{
	//Create initial AVLTree
	AVLTree<Author> authorsAVL;

	Author Aardvark("Anthony", "Aardvark");
	Author Aardvark2("Gregory", "Aardvark");

	Author Main("Micharl", "Main");
	Author McDowell("Gayle", "McDowell");

	authorsAVL.insert(Aardvark);
	authorsAVL.insert(Aardvark2);
	authorsAVL.insert(Main);
	authorsAVL.insert(McDowell);

	authorsAVL.printTree();

	std::cout << "Removing " << Main << std::endl;
	authorsAVL.remove(Main);

	return 0;
}
