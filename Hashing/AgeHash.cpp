/**********************************************
* File: AgeHash.cpp
* Author: Anni Chen
* Email: achen4@nd.edu
*
* Shows the comparison of the input or ordered and
* unordered sets
**********************************************/
// Libraries Here
#include <map>
#include <unordered_map>
#include <iterator>
#include <iostream>
#include <string>

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function. Solution
********************************************/
int main(int argc, char** argv) {

	std::map<std::string, int> ageHashOrdered = { {"Mattew",38},{"Alfred",72},{"Roscoe",36},{"James",35} };

	std::map<std::string, int> ::iterator iterOr;

	std::cout << "The Ordered hashes are: " << std::endl;

	for (iterOr = ageHashOrdered.begin(); iterOr != ageHashOrdered.end(); iterOr++) {
		std::cout << iterOr->first << " " << iterOr->second << std::endl;
	}

	std::unordered_map<std::string, int> ageHashUnordered = { {"Mattew",38},{"Alfred",72},{"Roscoe",36},{"James",35} };
	std::unordered_map<std::string, int>::iterator iterUn;

	std::cout << "--------" << std::endl << "The Unordered Hashes are:" << std::endl;

	for (iterUn = ageHashUnordered.begin(); iterUn != ageHashUnordered.end(); iterUn++) {
		std::cout << iterUn->first << " " << iterUn->second << std::endl;
	}

	std::cout << "The Ordered Example: "<<ageHashOrdered["Mattew"] << std::endl;
	std::cout << "The Unordered Example: " << ageHashUnordered["James"] << std::endl;

	return 0;
}
